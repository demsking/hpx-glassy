# Glassy Chrome Plasma Theme

A minimalist theme for KDE Plasma based on Glassy and ChromeOS themes

![GlassyChrome preview](theme/preview/preview.png)

## Credits

- Original Glassy theme [glassy-kde](https://github.com/Pr0cella/glassy-kde)
- Icons from [ChromeOS plasma theme](https://github.com/vinceliuice/ChromeOS-kde)

Individual licenses may apply

## License

Under the CC BY-SA 4.0 license. See [LICENSE](https://gitlab.com/demsking/hpx-glassy/blob/master/LICENSE) file for more details.
